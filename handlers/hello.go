package handlers

import (
	"fmt"
	"io"
	"log"
	"net/http"
)

type Hello struct {
	l *log.Logger
}

func NewHello(l *log.Logger) *Hello {
	return &Hello{l}
}

func (h *Hello) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.l.Println("hello world")
	d, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "opps", http.StatusBadGateway)
		return
	}
	h.l.Printf("Data %s\n", d)
	fmt.Fprintf(w, "hello %s", d)

}
